﻿using Microsoft.AspNetCore.StaticFiles;

namespace MuranoWeb.Utils
{
    public static class FileHelper
    {
        public static string GetContentType(string fileName)
        {
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(fileName, out contentType);
            return contentType ?? "application/octet-stream";
        }

        public static void DeleteFile(string directory, string fileName)
        {
            string path = System.IO.Path.Combine(directory, fileName);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
    }
}