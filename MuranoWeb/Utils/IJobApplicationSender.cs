﻿using System.Threading.Tasks;
using MuranoWeb.Data.Entity;

namespace MuranoWeb.Utils
{
    public interface IJobApplicationSender
    {
        Task Send(JobApplication jobApplication);
    }
}