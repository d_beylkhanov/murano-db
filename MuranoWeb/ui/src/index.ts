declare global {
    interface Window {
        google: any;
        officeMapCoordinates: { lat: number, lng: number };
    }
}

const baguetteBox = require('baguettebox.js');
import './scss/app.scss';
import {getElement, eventOn, document} from './app/DomUtils';
import PageNavigation from './app/PageNavigation';
import JobApplicationComponent from './app/JobApplicationComponent';
import VacancyComponent from './app/VacancyComponent';
import ChartComponent from './app/ChartComponent';
import OfficesMapComponent from './app/OfficesMapComponent';
import HeaderMenuComponent from './app/HeaderMenuComponent';
import MapComponent from './app/MapComponent';
import ScreenComponent from './app/ScreenComponent';
import TechnologyComponent from './app/TechnologyComponent';

/**
 * @function factory
 * @param {Function} component
 * @param {string} selector
 * @return {T}
 */
function factory<T>(component: { new (container: Element): T }, selector: string): T {
    let container: Element = getElement(selector);
    return container ? new component(container) : null;
}

/**
 * @function bindJobApplication
 * @param {JobApplicationComponent} JobApplication
 * @param {VacancyComponent} Vacancy
 */
function bindJobApplication(JobApplication: JobApplicationComponent, Vacancy: VacancyComponent): void {
    let buttonOpenJobApplication: Element = getElement('[data-js*="jobApplication"]');
    if (JobApplication) {
        if (buttonOpenJobApplication) {
            eventOn(buttonOpenJobApplication, 'click', (e: Event) => {
                let url: string = buttonOpenJobApplication.getAttribute('data-url');
                e.preventDefault();
                JobApplication.open(url);
            })
        }
        if (Vacancy) {
            Vacancy.setOpenCallback((url: string) => JobApplication.open(url));
        }
    }
}

/**
 * @function setupViewport
 */
function setupViewport(): void {
    let viewport: HTMLMetaElement = <HTMLMetaElement>getElement('#viewport');
    function changeViewport(): void {
        if (window.screen.width >= 480) {
            viewport.content = 'width=device-width, initial-scale=1.0';
            return;
        }
        viewport.content = 'width=480, shrink-to-fit=no';
    }
    if (viewport) {
        window.onresize = changeViewport;
        changeViewport();
    }
}

/**
 * @function run
 */
function run(): void {

    let AddressMap: MapComponent = factory(MapComponent, '[data-js*="address"]');
    let JobApplication: JobApplicationComponent = factory(JobApplicationComponent, '[data-js*="popover"]');
    let Vacancy: VacancyComponent = factory(VacancyComponent, '[data-js*="vacancy"]');
    let Screen: ScreenComponent = factory(ScreenComponent, '[data-js*="screen"]');
    bindJobApplication(JobApplication, Vacancy);

    factory(ChartComponent, '[data-js*="plot"]');
    factory(OfficesMapComponent, '[data-js*="officesMap"]');
    factory(HeaderMenuComponent, '[data-js*="menu"]');
    factory(TechnologyComponent, '[data-js="technology"]');

    if (AddressMap && window.google && window.google.maps && window.google.maps.Map && window.google.maps.Marker) {
        AddressMap.setConstructors(window.google.maps);
        AddressMap.initMapWithMarker(window.officeMapCoordinates);
    }

    if (Screen) {
        Screen.setScrollHandler(PageNavigation.scrollTo);
    }

    new PageNavigation();
    baguetteBox.run('.section__gallery-list');
    setupViewport();

}

eventOn(document, 'DOMContentLoaded', run);
