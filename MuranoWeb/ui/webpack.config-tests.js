/**
 * Module dependencies.
 * @private
 */
var path = require('path');
var nodeExternals = require('webpack-node-externals');

/**
 * Environment constants.
 * @private
 */
var ENV_COVERAGE = 'coverage';

/**
 * Module constants.
 * @private
 */
var env = process.env.NODE_ENV;
var srcDir = path.resolve(__dirname, 'src');
var coverageLoaders = [];

if (env === ENV_COVERAGE) {
    coverageLoaders = {
        test: /\.(js|ts)/,
        include: srcDir,
        loader: 'istanbul-instrumenter-loader'
    }
}

/**
 * Module configuration
 * @type {Object}
 * @constant
 * @private
 */
var moduleConfig = {
    loaders: [].concat(
        coverageLoaders,
        {
            test: /\.ts$/,
            exclude: /node_modules/,
            loader: 'ts-loader'
        }
    )
};

/**
 * Webpack configuration
 * @module WebpackConfigTests
 */
module.exports = {
    output: {
        devtoolModuleFilenameTemplate: '[absolute-resource-path]',
        devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
    },
    module: moduleConfig,
    target: 'node',
    externals: [
        nodeExternals()
    ],
    devtool: "inline-cheap-module-source-map"
};
