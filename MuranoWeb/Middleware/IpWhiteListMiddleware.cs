﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MuranoWeb.Filters
{
    public class IpWhiteListMiddleware : AuthorizeAttribute
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<IpWhiteListMiddleware> _logger;
        private readonly string[] _segments;
        private readonly string[] _adminWhiteList;

        public IpWhiteListMiddleware(RequestDelegate next, ILogger<IpWhiteListMiddleware> logger, string segments, string adminWhiteList)
        {
            _segments = segments.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries );
            _adminWhiteList = adminWhiteList.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            if (_segments.Any(x => context.Request.Path.StartsWithSegments(x)))
            {
                var userIp = context.Connection.RemoteIpAddress;
                if(userIp == null || _adminWhiteList.All(x => x.IndexOf(userIp.ToString(), StringComparison.OrdinalIgnoreCase) == - 1))
                {
                    _logger.LogInformation($"Forbidden Request from Remote IP address: {userIp}");
                    context.Response.StatusCode = (int)HttpStatusCode.Forbidden;

                    return;
                }
            }

            await _next.Invoke(context);
        }
    }
}
