﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MuranoWeb.Middleware
{
    public class OnlyLocalhostAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!("::1".Equals(context.HttpContext.Connection.RemoteIpAddress.ToString()) ||
                "127.0.0.1".Equals(context.HttpContext.Connection.RemoteIpAddress.ToString())))
            {
                context.Result = new StatusCodeResult(403);
                return;
            }
            base.OnActionExecuting(context);
        }
    }
}
