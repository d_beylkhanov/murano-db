﻿namespace MuranoWeb.Api.Models
{
    public class ImageModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool Show { get; set; }
    }
}