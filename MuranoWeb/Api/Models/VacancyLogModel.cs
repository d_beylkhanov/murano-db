﻿using MuranoWeb.Data.Entity;
using System;

namespace MuranoWeb.Api.Models
{
    public class VacancyLogModel
    {
        public VacancyLogModel(VacancyLog log)
        {
            Action = log.Action.ToString();
            User = log.User;
            Date = log.Date;
        }

        public string Action { get; private set; }
        public string User { get; private set; }
        public DateTime Date { get; private set; }
    }
}
