﻿namespace MuranoWeb.Api.Models
{
    public class LocationModel
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
