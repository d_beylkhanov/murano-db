﻿using System;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Data.Entity;
using System.Threading.Tasks;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Models.Home;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;
using MuranoWeb.Utils;
using Microsoft.AspNetCore.Hosting;
using NLog;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MuranoWeb.Config;

namespace MuranoWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<JobApplication> _jobApplicationsRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;
        private readonly IRepository<Quote> _quotesRepository;
        private readonly IRepository<Image> _imagesRepository;
        private readonly IRepository<VacancyIcon> _iconsRepository;
        private readonly IRepository<IpStatistic> _statisticRepository;
        private readonly JobApplicationOptions _applicationOptions;

        private readonly IJobApplicationSender _applicationSender;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public HomeController(IRepository<JobApplication> jobApplicationsRepository,
            IRepository<Vacancy> vacancyRepository, 
            IRepository<Quote> quotesRepository, 
            IRepository<Image> imagesRepository, 
            IRepository<VacancyIcon> iconsRepository, 
            IRepository<IpStatistic> statisticRepository, 
            IJobApplicationSender applicationSender,
            IOptions<JobApplicationOptions> applicationOptions)
        {
            _jobApplicationsRepository = jobApplicationsRepository;
            _vacancyRepository = vacancyRepository;
            _quotesRepository = quotesRepository;
            _imagesRepository = imagesRepository;
            _applicationSender = applicationSender;
            _iconsRepository = iconsRepository;
            _statisticRepository = statisticRepository;
            _applicationOptions = applicationOptions.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Pda()
        {
            return View();
        }

        public IActionResult Office(Location location)
        {
            try
            {
                if (Enum.IsDefined(typeof(Location), location))
                {
                    ViewBag.GalleryLinksList = _imagesRepository.GetAll().Where(x => x.Show && x.Location == location).OrderBy(x => x.SortOrder)
                       .Take(ImagesHelper.ImagesLimit).Select(x => new TwoLinksModel() { FullImage = ImagesHelper.GetImageUrl(x.Name, location), PreviewImage = ImagesHelper.GetImagePreviewUrl(x.PreviewName, location) });

                    var vacancies = _vacancyRepository.GetAll()
                        .Where(x => (x.Location & location) == location && !x.InArchive).OrderBy(x => x.Order).Select(x => new VacancyModel() { VacancyId = x.VacancyId, Description = x.Description, Location = x.Location, Order = x.Order, Requirements = x.Requirements, Summary = x.Summary, Title = x.Title, IconUrl = ImagesHelper.GetIconUrl(x.Icon) }).ToArray();

                    var quote = _quotesRepository.GetAll().OrderBy(x => Guid.NewGuid()).FirstOrDefault();

                    return View($"Offices/{location}", new OfficeModel
                    {
                        Vacancies = vacancies,
                        Quote = quote,
                        Location = location,
                    });
                }
                else
                {
                    return RedirectToAction("Office", new { location = Location.SaintPetersburg.ToString().ToLower() });
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occured preparing office info");
                return RedirectToAction("index", "home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostApplication(Location location, int? vacancyId, [FromForm]JobApplicationModel model)
        {
            try
            {
                var clientIp = Request.HttpContext.Connection.RemoteIpAddress;
                if (clientIp != null)
                {
                    var status = IPFilterHelper.CheckStatus(clientIp.ToString(), _statisticRepository);
                    if (status == UserStatus.Banned || status == UserStatus.TemporaryBanned)
                        return new ForbidResult();
                }
                if (ModelState.IsValid)
                {
                    if (model.CV != null)
                    {
                        if (!ValidateFile(model.CV))
                            return new ForbidResult();
                    }

                    var jobApplication = new JobApplication
                    {
                        FirstName = model.Application.FirstName,
                        LastName = model.Application.LastName,
                        Email = model.Application.Email,
                        Comment = model.Application.Comment,
                        Location = location,
                        VacancyId = vacancyId,
                        FileName = model.CV?.FileName,
                        FileContent = await getFileContent(model.CV),
                        CreatedOn = DateTime.Now
                    };
                    await _jobApplicationsRepository.Add(jobApplication);
                    await _applicationSender.Send(jobApplication);
                    return new StatusCodeResult(200);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "PostApplication");
            }
            return new StatusCodeResult(500);
        }

        private bool ValidateFile(IFormFile file)
        {
            if (!string.IsNullOrEmpty(file.FileName))
            {
                var extension = System.IO.Path.GetExtension(file.FileName);
                if (!string.IsNullOrEmpty(extension))
                {
                    return _applicationOptions.IsExtensionValid(extension);
                }
                else
                    return true;
            }
            return true;
        }
        private async Task<byte[]> getFileContent(IFormFile file)
        {
            _logger.Info("getFileContent");
            if (file != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await file.CopyToAsync(memoryStream).ConfigureAwait(false);
                    return memoryStream.ToArray();
                }
            }
            return null;
        }
    }
}