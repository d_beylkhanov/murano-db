import { MuranoAdminPage } from './app.po';

describe('murano-admin App', () => {
  let page: MuranoAdminPage;

  beforeEach(() => {
    page = new MuranoAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
