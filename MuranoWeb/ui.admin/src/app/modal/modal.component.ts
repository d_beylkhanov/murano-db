import { Component } from '@angular/core';
import { EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  public visible = false;
  public visibleAnimate = false;

  @Output()
  open: EventEmitter<any> = new EventEmitter();
  @Output()
  close: EventEmitter<any> = new EventEmitter();

  public show(): void {
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);

    this.open.emit(null);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);

    this.close.emit(null);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }
}
