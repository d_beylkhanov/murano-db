import { Component, OnInit } from '@angular/core';
import { VacanciesService } from '../services/vacancies.service';
import { Vacancy } from '../services/vacancy';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from '../modal/modal.component';
import { VacancyLog } from '../services/vacancy.log';
import {OfficeLocation} from '../services/office.location';
import {Sortable} from '../decorators/sortable.decorator';

@Component({
  selector: 'app-vacancies-list',
  templateUrl: './vacancies-list.component.html',
  styleUrls: ['./vacancies-list.component.css']
})
@Sortable('vacancies', 'onSort')
export class VacanciesListComponent implements OnInit {
  vacancies: Vacancy[];
  logs: VacancyLog[];
  inArchive: boolean;
  location: OfficeLocation;

  constructor(private service: VacanciesService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.inArchive = this.route.snapshot.data['inArchive'];

  }

  locationSelect(location: OfficeLocation) {
    this.location = location;
    this.loadVacancies();
  }

  loadVacancies() {
    this.service.getAll({ location: this.location.code.toString() })
                .then(v => this.vacancies = v.filter(f => f.inArchive === this.inArchive));
  }

  showLog(vacancy: Vacancy, modal: ModalComponent) {
    this.service.getLogs(vacancy.id).then(x => {
      this.logs = x;
      modal.show();
    });
  }

  onSort() {
    this.service.sortVacancies(this.location.code, this.vacancies)
                .then(() => this.loadVacancies());
  }

  closeLogs() {
    this.logs = [];
  }
}
