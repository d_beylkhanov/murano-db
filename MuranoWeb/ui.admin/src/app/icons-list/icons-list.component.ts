﻿import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { Icon } from '../services/icon';
import { IconService } from '../services/icons.service';

@Component({
  selector: 'app-icons-list',
  templateUrl: './icons-list.component.html',
  styleUrls: ['./icons-list.component.css']
})
export class IconsListComponent implements OnInit {
    icons: Icon[];
    currentIconId: number;
    clickedIconId: number;
    @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private service: IconService) { }

  ngOnInit() {
      this.currentIconId = -1;
      this.loadIconsList();
  }

  getIconById(iconId: number): Icon {
      if (iconId < 0)
          return null;
      return this.icons.find(x => x.id == iconId);
  }

  editIcon(icon: Icon) {
      if (this.currentIconId >= 0) {
          this.saveIcon(icon.id);
      }
      //this.editingIcon = { id: icon.id, url: icon.url, title: icon.title };
      this.currentIconId = icon.id;
  }

  closeEdit(icon: Icon) {
      this.currentIconId = -1;
      this.loadIconsList();
  }

  deleteIcon(icon: Icon) {
      if (icon.id == this.currentIconId) {
          this.currentIconId = -1;
      }
      this.service.deleteIcon(icon)
          .then(() => this.loadIconsList());
  }

  setIcons(ics: Icon[]) {
      this.icons = ics;
  }

  loadIconsList() {
      this.service.getIcons()
          .then(v => this.setIcons(v));
  }

  filesChoose(files: FileList) {
      this.service.uploadIcons(files)
          .then(() => this.loadIconsList());
  }

  saveIcon(id: number) {
      let icon: Icon = this.getIconById(id);

      if (icon != null) {
          if (icon.title !== '') {
              const promise = this.service.update(icon.id, icon);
              this.closeEdit(icon);
          }
          else {
              this.closeEdit(icon);
          }
      }
  }

  addImage(event) {
      let files = event.target.files;
      if (this.clickedIconId == -1) {
          this.service.uploadIcons(files)
              .then(() => this.loadIconsList());
      }
      else {
          this.service.replaceIcon(this.getIconById(this.clickedIconId), files)
              .then(() => this.loadIconsList());
      }
  }

  addNewIcon() {
      this.clickedIconId = -1;
      this.fileInput.nativeElement.click();
  }

  replaceIcon(icon: Icon) {
      this.clickedIconId = icon.id;
      this.fileInput.nativeElement.click();
  }

  saveIconUI(icon: Icon) {
    if (icon.title !== '') {
        const promise = this.service.update(icon.id, icon);
        this.closeEdit(icon);
    }
  }
}

