import {Component, OnInit} from '@angular/core';
import {Photo} from '../services/photo';
import {PhotoService} from '../services/photo.service';
import {OfficeLocation} from '../services/office.location';
import {Sortable} from '../decorators/sortable.decorator';

@Component({
  selector: 'app-photos-list',
  templateUrl: './photos-list.component.html',
  styleUrls: ['./photos-list.component.css']
})
@Sortable('photos', 'onSort')
export class PhotosListComponent implements OnInit {
  photos: Photo[];
  location: OfficeLocation;
  lightboxPhoto: Photo;

  constructor(private service: PhotoService) {
  }

  ngOnInit() {
  }

  locationSelect(location: OfficeLocation) {
      this.location = location;
      this.loadPhotosList();
  }

  filesChoose(files: FileList) {
    this.service.uploadPhotos(this.location.code, files)
                .then(() => this.loadPhotosList());
  }

  toggleShow(photo: Photo) {
    photo.show = !photo.show;
    this.service.toggleShow(this.location.code, photo)
                .then(() => this.loadPhotosList());
  }

  deletePhoto(photo: Photo) {
    this.service.deletePhoto(this.location.code, photo)
                .then(() => this.loadPhotosList());
  }

  loadPhotosList() {
    this.service.getPhotos(this.location.code)
                .then(v => this.photos = v);
  }

  onSort() {
    this.service.sortPhotos(this.location.code, this.photos)
                .then(() => this.loadPhotosList());
  }

}
