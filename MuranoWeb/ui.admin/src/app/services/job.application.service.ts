import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DataService } from './data.service';
import { JobApplication } from './job.application';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class JobApplicationService extends DataService<JobApplication> {
    constructor(http: Http) { super(http, 'admin/api/applications'); }
}
