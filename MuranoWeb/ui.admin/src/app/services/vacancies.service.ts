﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Vacancy } from './vacancy';
import { DataService } from './data.service';
import { VacancyLog } from './vacancy.log';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class VacanciesService extends DataService<Vacancy> {
    constructor(http: Http) { super(http, 'admin/api/vacancies'); }

    public save(vacancy: Vacancy): Promise<Vacancy> {
        return vacancy.id ? super.update(vacancy.id, vacancy) : super.add(vacancy);
    }

    public toArchive(id: number): Promise<Vacancy> {
        return this.http.delete(`${this.url}/${id}`).toPromise()
            .then(response => response.json() as Vacancy).catch(super.handleError);
    }

    public fromArchive(id: number): Promise<Vacancy> {
        return this.http.options(`${this.url}/${id}`).toPromise()
            .then(response => response.json() as Vacancy).catch(super.handleError);
    }

    public getLogs(id: number): Promise<VacancyLog[]> {
        return this.http.get(`${this.url}/${id}/log`).toPromise()
            .then(response => response.json() as VacancyLog[]).catch(super.handleError);
    }

    public sortVacancies(locationCode: number, vacancies: Vacancy[]) {
        return this.http.patch(`${this.url}/sort/${locationCode}`, vacancies.map(v => v.id)).toPromise()
            .catch(this.handleError);
    }
}
