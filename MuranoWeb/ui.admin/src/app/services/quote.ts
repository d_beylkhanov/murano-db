export class Quote {
    id: number;
    author: string;
    text: string;
}
