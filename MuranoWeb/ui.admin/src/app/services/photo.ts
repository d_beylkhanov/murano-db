﻿export class Photo {
  id: number;
  url: string;
  show: boolean;
}
