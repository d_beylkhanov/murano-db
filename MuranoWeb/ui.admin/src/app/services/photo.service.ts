import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Headers} from '@angular/http';
import { Photo } from './photo';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class PhotoService {
  private url = 'admin/api/images';  // URL to web api

  constructor(private http: Http) {}

  public getPhotos(locationCode: number): Promise<Photo[]> {
    return this.http.get(`${this.url}/${locationCode}`)
                    .toPromise()
                    .then(response => response.json() as Photo[])
                    .catch(this.handleError);
  }

  public uploadPhotos(locationCode: number, files: FileList): Promise<any> {
    let args: RequestOptionsArgs = <RequestOptionsArgs>{
      headers: new Headers({
        'Content-Type': 'multipart/form-data; charset=UTF-8'
      })
    };
    return this.http.post(`${this.url}/${locationCode}`, this.makeFormData(files), args)
                    .toPromise()
                    .catch(this.handleError);
  }

  public toggleShow(locationCode: number, photo: Photo) {
    return this.http.patch(`${this.url}/${locationCode}/${photo.id}`, {})
                    .toPromise()
                    .catch(this.handleError);
  }

  public sortPhotos(locationCode: number, photos: Photo[]) {
    return this.http.patch(`${this.url}/sort/${locationCode}`, photos.map(v => v.id))
                    .toPromise()
                    .catch(this.handleError);
  }

  public deletePhoto(locationCode: number, photo: Photo) {
    return this.http.delete(`${this.url}/${photo.id}`)
                    .toPromise()
                    .catch(this.handleError);
  }

  private makeFormData(files: FileList): FormData {
    let formData: FormData = new FormData();
    for (let i: number = 0; i < files.length; i++) {
      formData.append('files', files[i], files[i].name);
    }
    return formData;
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
