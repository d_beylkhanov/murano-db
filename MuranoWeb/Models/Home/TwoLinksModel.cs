﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Models.Home
{
    public class TwoLinksModel
    {
        public string FullImage { get; set; }
        public string PreviewImage { get; set; }
    }
}
